package io.learning.rest.controller.controller.user;

import io.learning.model.user.User;
import io.learning.service.exception.BusinessException;
import io.learning.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/user")
public class UserController {

    private UserService userService;

    public UserController() {
    }

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/id/{id}")
    public ResponseEntity findById(@PathVariable Long id) {

        return ResponseEntity.ok(userService.findById(id));
    }

    @GetMapping(value = "/all")
    public ResponseEntity findAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @PostMapping(value = "/save")
    public ResponseEntity save(@RequestBody User user) {

        try {
            return ResponseEntity.ok(userService.save(user));
        } catch (BusinessException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping(value = "/update")
    public ResponseEntity update(@RequestBody User user) {
        try {
            return ResponseEntity.ok(userService.update(user));
        } catch (BusinessException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }


    //TODO : Since we don't want to delete a user we can create a column named 'deleted' in users table and
    // set his value to true when registered and to false when deleted and
    // when retrieving users we look for those where deleted = false
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
