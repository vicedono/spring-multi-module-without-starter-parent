package io.learning.rest.controller.controller.role;

import io.learning.model.role.Role;
import io.learning.service.exception.BusinessException;
import io.learning.service.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/role")
public class RoleController {

    private RoleService roleService;

    public RoleController() {
    }

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping(value = "/all")
    public ResponseEntity findAll() {
        return ResponseEntity.ok(roleService.findAll());
    }

    @PostMapping(value = "/save")
    public ResponseEntity save(@RequestBody Role role) {

        try {
            return ResponseEntity.ok(roleService.save(role));
        } catch (BusinessException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping(value = "/update")
    public ResponseEntity update(@RequestBody Role role) {

        try {
            return ResponseEntity.ok(roleService.update(role));
        } catch (BusinessException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    //TODO : In fact we cannot delete a role since there are other users with that/those role(s),
    // so this is just for testing must be removed after
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        roleService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
