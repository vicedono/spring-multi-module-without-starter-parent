package io.learning.rest.controller.model;

public class RestControllerConstant {

    public static final String SIGNING_KEY = "blog-app";

    public static final String TOKEN_PREFIX = "Bearer";

    public static final String HEADER_STRING = "Authorization";

    public static final String AUTHORITIES_KEY = "scopes";

    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5 * 60 * 60L;
}
