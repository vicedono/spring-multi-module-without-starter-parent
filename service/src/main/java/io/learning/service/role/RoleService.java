package io.learning.service.role;

import io.learning.model.role.Role;
import io.learning.service.exception.BusinessException;

import java.util.Collection;

public interface RoleService {

    Collection<Role> findAll();

    Role findByName(String name);

    Role save(Role role) throws BusinessException;

    Role update(Role role) throws BusinessException;

    void delete(Long id);

    Collection<Role> findAllRoles(Collection<Role> authorities) throws BusinessException;
}
