package io.learning.service.role;

import io.learning.dao.role.mapper.RoleMapper;
import io.learning.dao.role.model.RoleEntity;
import io.learning.dao.role.repository.RoleRepository;
import io.learning.model.role.Role;
import io.learning.model.core.ModelConstant;
import io.learning.service.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;
    private RoleMapper roleMapper;

    public RoleServiceImpl() {
    }

    //TODO : If i specify in MainClass of this module the baseScanPackage="io.learning" everything will be fine
    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository, RoleMapper roleMapper) {
        this.roleRepository = roleRepository;
        this.roleMapper = roleMapper;
    }

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Collection<Role> findAll() {
        return roleMapper.mapToModelCollection(roleRepository.findAll());
    }

    @Override
    public Role findByName(String name) {
        RoleEntity roleEntity = roleRepository.findByName(name);

        return Objects.nonNull(roleEntity) ? roleMapper.mapToModel(roleEntity) : null;
    }

    @Override
    public Role save(Role role) throws BusinessException {

        if (Objects.isNull(role)) {
            throw new BusinessException(ModelConstant.ROLE_CANNOT_BE_NULL);
        }

        RoleEntity existingRole = roleRepository.findByName(role.getName());
        if (!Objects.isNull(existingRole)) {
            throw new BusinessException(ModelConstant.ROLE_ALREADY_EXISTS + "(the RoleName : " + existingRole.getName() + "(id = " + existingRole.getId() + "))");
        }

        RoleEntity roleEntityToSave = roleMapper.mapToEntity(role);
        roleEntityToSave = roleRepository.save(roleEntityToSave);

        return roleMapper.mapToModel(roleEntityToSave);
    }

    @Override
    public Role update(Role role) throws BusinessException {

        if (Objects.isNull(role)) {
            throw new BusinessException(ModelConstant.ROLE_CANNOT_BE_NULL);
        }

        Optional<RoleEntity> optional = roleRepository.findById(role.getId());

        if (!optional.isPresent()) {
            throw new BusinessException(ModelConstant.ROLE_DOESNT_EXISTS);
        }

        if (Objects.nonNull(roleRepository.findByNameAndIdNot(role.getName(), role.getId()))) {
            throw new BusinessException(ModelConstant.ROLE_ALREADY_EXISTS);
        }

        RoleEntity roleEntityToUpdate = optional.get();

        if(!role.getName().equals(roleEntityToUpdate.getName())) {
            roleEntityToUpdate.setName(role.getName());
            roleEntityToUpdate = roleRepository.save(roleEntityToUpdate);

        }

        return roleMapper.mapToModel(roleEntityToUpdate);
    }

    @Override
    public void delete(Long id) {

        Optional<RoleEntity> optional = roleRepository.findById(id);
        if (optional.isPresent()) {
            roleRepository.delete(optional.get());
        }
    }

    @Override
    public Collection<Role> findAllRoles(Collection<Role> authorities) throws BusinessException {

        Collection<Role> roles = new HashSet<>();

        if (Objects.isNull(authorities) || authorities.size() == 0) {
            roles.add(createRoleIfNotExist(ModelConstant.ROLE_USER));
        } else {

            for (Role role : authorities) {
                Role roleFounded = createRoleIfNotExist(role.getName());
                roles.add(roleFounded);
            }
        }

        return roles;
    }

    private Role createRoleIfNotExist(String roleName) throws BusinessException {

        Role role = findByName(roleName);

        if (Objects.isNull(role)) {
            role = save(new Role(roleName));
        }

        return role;
    }
}
