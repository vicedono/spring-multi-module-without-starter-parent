package io.learning.service.user;

import io.learning.model.user.User;
import io.learning.service.exception.BusinessException;

import java.util.Collection;

public interface UserService {

    User findById(Long id);

    Collection<User> findAll();

    User save(User user) throws BusinessException;

    User update(User user) throws BusinessException;

    //TODO: May be i should pass the username and/or the Token since and id can
    void delete(Long id);
}
