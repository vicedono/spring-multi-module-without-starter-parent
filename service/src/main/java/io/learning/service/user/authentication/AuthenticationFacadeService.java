package io.learning.service.user.authentication;

import org.springframework.security.core.Authentication;

public interface AuthenticationFacadeService {

    Authentication getAuthentication();

    String getName();
}
