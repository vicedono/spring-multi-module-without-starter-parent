package io.learning.service.user;

import io.learning.dao.user.entity.UserEntity;
import io.learning.dao.user.mapper.UserMapper;
import io.learning.dao.user.repository.UserRepository;
import io.learning.model.role.Role;
import io.learning.model.user.User;
import io.learning.model.core.ModelConstant;
import io.learning.service.exception.BusinessException;
import io.learning.service.role.RoleService;
import io.learning.service.user.authentication.AuthenticationFacadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

;

@Service
public class UserServiceImpl implements UserService {

    private UserMapper userMapper;
    private UserRepository userRepository;
    private RoleService roleService;
    private BCryptPasswordEncoder encoder;
    private AuthenticationFacadeService authenticationFacadeService;

    public UserServiceImpl() {
    }

    //TODO : If i specify in MainClass of this module the baseScanPackage="io.learning" everything will be fine
    @Autowired
    public UserServiceImpl(UserMapper userMapper, UserRepository userRepository, RoleService roleService, BCryptPasswordEncoder encoder, AuthenticationFacadeService authenticationFacadeService) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.roleService = roleService;
        this.encoder = encoder;
        this.authenticationFacadeService = authenticationFacadeService;
    }

    @Override
    public User findById(Long id) {

        Optional<UserEntity> optional = userRepository.findById(id);

        return optional.isPresent() ? userMapper.toModel(optional.get()) : null;
    }

    @Override
    public Collection<User> findAll() {
        return userMapper.toModelCollection(userRepository.findAll());
    }

    @Override
    public User save(User user) throws BusinessException {

        if (Objects.isNull(user)) {
            throw new BusinessException(ModelConstant.USER_CANNOT_BE_NULL);
        }

        if (Objects.isNull(user.getUsername()) || Objects.isNull(user.getEmail())) {
            throw new BusinessException(ModelConstant.USER_OR_EMAIL_CANNOT_BE_NULL);
        }

        if (!Objects.isNull(userRepository.findByUsernameOrEmail(user.getUsername(), user.getEmail()))) {
            throw new BusinessException(ModelConstant.USER_ALREADY_EXISTS);
        }

        //If the collection is empty,
        // then the user's role collection to a single role(ie: roleName = ROLE_USER)
        // otherwise create the role if it do not exists
        Collection<Role> roles = roleService.findAllRoles(user.getRoles());

        user.setRoles(roles);
        setSecurityParametersAndEncryptPassword(user);

        UserEntity userEntity = userMapper.toEntity(user);
        userEntity = userRepository.save(userEntity);

        return userMapper.toModel(userEntity);
    }

    @Override
    public User update(User user) throws BusinessException {

        if (Objects.isNull(user)) {
            throw new BusinessException(ModelConstant.USER_CANNOT_BE_NULL);
        }

        Optional<UserEntity> optional = userRepository.findById(user.getId());

        if (!optional.isPresent()) {
            throw new BusinessException(ModelConstant.USER_DOESNT_EXISTS);
        }

        List<UserEntity> existingUsers = userRepository.findByUsernameNotOrEmailNotAndIdNot(user.getUsername(), user.getEmail(), optional.get().getId());

        if (!existingUsers.isEmpty()) {
            throw new BusinessException(ModelConstant.USER_OR_EMAIL_ALREADY_EXISTS);
        }

        user.setId(optional.get().getId());

        UserEntity userToUpdate = userMapper.toEntity(user);
        userToUpdate = userRepository.save(userToUpdate);
        return userMapper.toModel(userToUpdate);
    }


    // A connected user can delete :
    //      himself only if he doesnt have the ROLE_ADMIN
    //      another one only if he has have the ROLE_ADMIN
    //TODO : Delete a user means to desactivate him
    @Override
    public void delete(Long id) {

        Optional<UserEntity> optional = userRepository.findById(id);

        if(Objects.nonNull(authenticationFacadeService.getName())) {

            if(optional.isPresent()) {
                UserEntity userToDelete = optional.get();

                boolean hasRoleAdmin = userToDelete.getRoles()
                        .stream()
                        .anyMatch(roleEntity -> roleEntity.getName().equals(ModelConstant.ROLE_ADMIN));

                boolean isPersonToBeDeletedIsConnected = userToDelete.getUsername().equals(authenticationFacadeService.getName());

                if((hasRoleAdmin && !isPersonToBeDeletedIsConnected) || (!hasRoleAdmin && isPersonToBeDeletedIsConnected)) {
                    userRepository.delete(optional.get());
                }
            }
        }
    }

    private void setSecurityParametersAndEncryptPassword(User user) {
        user.setAccountNonExpired(true);
        user.setEnabled(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setPassword(encoder.encode(user.getPassword()));
    }
}
