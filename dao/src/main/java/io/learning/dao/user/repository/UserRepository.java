package io.learning.dao.user.repository;

import io.learning.dao.user.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByUsername(String username);

    UserEntity findByEmail(String email);

    UserEntity findByUsernameOrEmail(String username, String email);

    List<UserEntity> findByUsernameNotOrEmailNotAndIdNot(String username, String email, Long id);
}
