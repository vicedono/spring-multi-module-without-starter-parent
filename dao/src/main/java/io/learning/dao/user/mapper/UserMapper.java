package io.learning.dao.user.mapper;

import io.learning.dao.role.mapper.RoleMapper;
import io.learning.dao.user.entity.UserEntity;
import io.learning.model.user.User;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring", uses = RoleMapper.class)
public interface UserMapper {

    UserEntity toEntity(User user);

    User toModel(UserEntity userEntity);

    Collection<User> toModelCollection(Collection<UserEntity> users);

    Collection<UserEntity> toEntityCollection(Collection<User> users);
}
