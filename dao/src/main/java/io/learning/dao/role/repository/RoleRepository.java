package io.learning.dao.role.repository;

import io.learning.dao.role.model.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    RoleEntity findByName(String name);

    RoleEntity findByNameAndIdNot(String name, Long id);
}
