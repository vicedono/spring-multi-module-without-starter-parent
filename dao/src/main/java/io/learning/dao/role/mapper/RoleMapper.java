package io.learning.dao.role.mapper;

import io.learning.dao.role.model.RoleEntity;
import io.learning.model.role.Role;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    Role mapToModel(RoleEntity roleEntity);

    RoleEntity mapToEntity(Role role);

    Collection<Role> mapToModelCollection(Collection<RoleEntity> roles);

    Collection<RoleEntity> mapToEntityCollection(Collection<Role> roles);
}
